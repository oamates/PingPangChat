package com.pingpang.websocketchat.send.impl;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pingpang.image.VideoUtil;
import com.pingpang.websocketchat.ChannelManager;
import com.pingpang.websocketchat.Message;
import com.pingpang.websocketchat.send.ChatSend;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ChatSendAudio extends ChatSend {

	private Map<String,String> audioSave=new ConcurrentHashMap<String,String>();
    private Map<String,String> audioHead=new ConcurrentHashMap<String,String>();
	@Override
	public void send(Message message, ChannelHandlerContext ctx) throws JsonProcessingException {
       
		if (null != ChannelManager.getChannel(message.getAccept().getUserCode())) {
			
		    Set<String> msg=new HashSet<String>();
			msg.add("request");
			msg.add("close");
			msg.add("refuse");
			msg.add("accept");
			
			//发送数据这里改为redis
			message.getFrom().setUserPassword("");
			message.getAccept().setUserPassword("");
			
			message.setFrom(userService.getUser(message.getFrom()));
			message.setAccept(userService.getUser(message.getAccept()));
			
			 ChannelManager.getChannel(message.getAccept().getUserCode())
		 	   .writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
			
			if("close".equals(message.getMsg())) {
				audioSave.remove(message.getFrom().getUserCode()+"-"+message.getAccept().getUserCode());
				audioSave.remove(message.getAccept().getUserCode()+"-"+message.getFrom().getUserCode());
				
				audioHead.remove(message.getFrom().getUserCode()+"-"+message.getAccept().getUserCode());
				audioHead.remove(message.getAccept().getUserCode()+"-"+message.getFrom().getUserCode());
				
				message.setCmd("3");
				message.setMsg("【视频聊天结束】");
				userMsgService.addMsg(message);
			}
			
			if("refuse".equals(message.getMsg())) {
				message.setCmd("3");
				message.setMsg("【视频聊天被拒绝】");
				userMsgService.addMsg(message);
			}
			if("3".equals(message.getCmd())) {
			   ChannelManager.getChannel(message.getAccept().getUserCode())
		 	   .writeAndFlush(new TextWebSocketFrame(this.getMapper().writeValueAsString(message)));
			}
		   
		   if(!msg.contains(message.getMsg())){
			    String fileNameKey=message.getFrom().getUserCode()+"-"+message.getAccept().getUserCode();
				//logger.info("IP:{},用户:{},对方用户:{}多媒体错误信息{}",ctx.channel().remoteAddress(),message.getFrom().getUserCode(),message.getAccept().getUserCode(),message.getMsg());
				//return;
				//保存下文件
				//String fileName=audioSave.get(message.getFrom().getUserCode()+"-"+message.getAccept().getUserCode());
				//if(null==fileName) {
			        String type=".mp4";//.webm .mkv
					String uuid=UUID.randomUUID().toString().replace("-", "");
					String fileName=PATH+uuid+fileNameKey+type;
			    //audioSave.put(message.getFrom().getUserCode()+"-"+message.getAccept().getUserCode(), fileName);
				//}
				try {
					logger.info("{}->保存路径:{}",fileNameKey,fileName);
//					String head=audioHead.get(fileNameKey);
//					if(null==head) {
//						head=VideoUtil.getHead(message.getMsg(), 0, 189);
//					    audioHead.put(fileNameKey, head);
//						logger.info(fileNameKey+":"+head);
//						VideoUtil.decoderBase64File(message.getMsg(), fileName, PATH);
//					}else {
//						logger.info(fileNameKey+":"+head);
//						VideoUtil.decoderBase64FileAddHead(head,message.getMsg(), fileName, PATH);
//					}
					//VideoUtil.decoderBase64File(message.getMsg(), fileName, PATH);
				} catch (Exception e) {
					logger.error("视频保存出错!", e);
				}
			}	   
	    }
	}

}
